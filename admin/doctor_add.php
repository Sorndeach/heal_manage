<?php
    session_start();
    include('../dbconn.inc.php');

    if( $_POST ){
        $doctor_id = $_POST['doctor_id'];
        $doctor_name = $_POST['doctor_name'];
        $doctor_phone = $_POST['doctor_phone'];
        $doctor_add = $_POST['doctor_add'];

        $sql = "SELECT * FROM doctor WHERE doctor_id = '$doctor_id'";
        $query = $mysqli->query($sql);
        if($query->num_rows){
            exit("<script>alert('ID ของท่านซ้ำครับ');history.back();</script>");
        }

        $sql = "INSERT INTO doctor VALUES('$doctor_id','$doctor_name','$doctor_add','$doctor_phone')";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="doctor_list.php";
        </script>
<?php
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <?include('_navtop.php');?>
        <div class="row" style="padding: 50px;">
            <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-success">
                    <div class="panel-heading">เพิ่มแพทย์แผนโบราณ</div>
                    <div class="panel-body">
                        <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                            <div class="form-group">
                                <label>รหัส</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-braille"></i></span>
                                    <input type="text" name="doctor_id" class="form-control" id="doctor_id" style="width:50%" value="" placeholder="ระบุรหัส" maxlength="200" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ชื่อ-นามสกุล</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="doctor_name" class="form-control" id="doctor_name" style="width:50%" placeholder="ระบุชื่อ-นามสกุล" maxlength="200" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>โทรศัพท์</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                                    <input type="text" name="doctor_phone" class="form-control" id="doctor_phone" style="width:50%" placeholder="ระบุโทรศัพท์" maxlength="10" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ที่อยู่</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                    <textarea class="form-control" name="doctor_add" placeholder="ระบุที่อยู่" maxlength="500"></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">ตกลง</button>
                            <button type="reset" class="btn btn-warning">ยกเลิก</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>