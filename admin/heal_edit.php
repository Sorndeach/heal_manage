<?php
    session_start();
    include('../dbconn.inc.php');

    if($_GET){
        $heal_id = $_GET['heal_id'];
        $data_edit = query1("SELECT * FROM heal WHERE heal_id = '$heal_id'");
    }
    if($_POST){
        $heal_id = $_POST['heal_id'];
        $heal_name = $_POST['heal_name'];
        $doctor_id = $_POST['doctor_id'];
        $mtype_id = $_POST['mtype_id'];
        $heal_date = $_POST['heal_date'];
        $heal_detail = $_POST['heal_detail'];

        $sql = "UPDATE heal 
        SET heal_name = '$heal_name',
        doctor_id = '$doctor_id',
        mtype_id = '$mtype_id',
        heal_date = '$heal_date',
        heal_detail = '$heal_detail'
        WHERE heal_id = '$heal_id'";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="heal_list.php";
        </script>
<?php
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <?include('_navtop.php');?>
        <div class="row" style="padding: 50px;">
            <div class="col-md-6 col-md-offset-3">
                <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                    <div class="col-md-12">
                        <table style="width: 100%;" class="custom-ta">
                            <tr>
                                <th colspan="2"><span>เพิ่มข้อมูลโรค</span></th>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><span>รหัส :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-braille"></i></span>
                                        <input type="text" name="heal_id"  class="form-control" id="heal_id" value="<?php echo $data_edit->heal_id; ?>" placeholder="ระบุรหัส"  maxlength="200" readonly>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><span>ชื่อโรค :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-area-chart"></i></span>
                                        <input type="text" name="heal_name" class="form-control" id="heal_name" value="<?php echo $data_edit->heal_name; ?>" placeholder="ระบุชื่อโรค"  maxlength="200" required>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><span>แพทย์แผนโบราณ :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <select class="form-control" name="doctor_id">
                                            <option value="">:::เลือกแพทย์แผนโบราณ:::</option>
                                            <?php
                                                $query = query2("SELECT * FROM doctor");
                                                while($data = $query->fetch_object()) {
                                            ?>
                                                 <option value="<?php echo $data->doctor_id; ?>" <?php echo $data_edit->doctor_id == $data->doctor_id ? 'selected' : ''; ?> ><?php echo $data->doctor_name; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><span>ประเภทการรักษา :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-sticky-note-o"></i></span>
                                        <select class="form-control" name="mtype_id">
                                            <option value="">:::เลือกประเภทการรักษา:::</option>
                                            <?php
                                                $query = query2("SELECT * FROM medtype");
                                                while($data = $query->fetch_object()) {
                                            ?>
                                                 <option value="<?php echo $data->mtype_id; ?>" <?php echo $data_edit->mtype_id == $data->mtype_id ? 'selected' : ''; ?> ><?php echo $data->mtype_name; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><span>วันที่ :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="date" name="heal_date" class="form-control" value="<?php echo $data_edit->heal_date; ?>" id="heal_date" maxlength="200" required>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><span>รายละเอียด :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                        <textarea name="heal_detail" class="form-control" id="heal_detail" placeholder="ระบุรายละเอียด" maxlength="500" required><?php echo $data_edit->heal_detail; ?></textarea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <button type="submit" class="btn btn-primary">ตกลง</button>
                                    <a href="heal_list.php" class="btn btn-warning">ยกเลิก</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
            
        </div>
    </body>
</html>