

<?php
    session_start();
    include('../dbconn.inc.php');

    if( isset($_POST['patient_name']) && !empty($_POST['patient_name']) ){
        $patient_id = $_POST['patient_id'];
        $patient_name = $_POST['patient_name'];
        $patient_phone = $_POST['patient_phone'];
        $patient_add = $_POST['patient_add'];
        $for = $_POST['for'];

        if( !empty($for) && $for == 'edit' ){
            $sql = "UPDATE patient SET 
            patient_name = '$patient_name', 
            patient_add = '$patient_add',
            patient_phone = '$patient_phone'
            WHERE patient_id = '$patient_id'";
            @$mysqli->query($sql) or die($mysqli->error);
        }else{
            $sql = "INSERT INTO patient VALUES('$patient_id','$patient_name','$patient_add','$patient_phone')";
            @$mysqli->query($sql) or die($mysqli->error);
        }
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="patient_list.php";
        </script>
<?php
    }

    if($_GET){
        $patient_id = $_GET['patient_id'];
        $for = $_GET['for'];

        if( $for == 'edit' ){
            $data = query1("SELECT * FROM patient WHERE patient_id = '$patient_id'");
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <?include('_navtop.php');?>
        <div class="row" style="padding: 50px;">
            <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-success">
                    <div class="panel-heading">เพิ่มข้อมูลคนไข้</div>
                    <div class="panel-body">
                        <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                            <div class="form-group">
                                <label>รหัส</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-braille"></i></span>
                                    <input type="text" name="patient_id" class="form-control" id="patient_id" value="<?php echo isset($data->patient_id)? $data->patient_id : ''; ?>"  style="width:50%" placeholder="ระบุรหัส" maxlength="200" <?php echo isset($data->patient_id)? 'readonly' : 'required'; ?> >
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ชื่อ-นามสกุล</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="patient_name" class="form-control" id="patient_name" value="<?php echo isset($data->patient_name)? $data->patient_name : ''; ?>" style="width:50%" placeholder="ระบุชื่อ-นามสกุล" maxlength="200" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>โทรศัพท์</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                                    <input type="text" name="patient_phone" class="form-control" id="patient_phone" value="<?php echo isset($data->patient_phone)? $data->patient_phone : ''; ?>" style="width:50%" placeholder="ระบุโทรศัพท์" maxlength="10" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ที่อยู่</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                    <textarea class="form-control" name="patient_add" placeholder="ระบุที่อยู่" maxlength="500"><?php echo isset($data->patient_add)? $data->patient_add : ''; ?></textarea>
                                </div>
                            </div>
                            <input type="hidden" name="for" value="<?php echo isset( $for )? $for : ''; ?>">
                            <button type="submit" class="btn btn-primary">ตกลง</button>
                            <button type="reset" class="btn btn-warning">ยกเลิก</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>