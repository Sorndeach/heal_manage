<?php
    session_start();
    include('../dbconn.inc.php');

    if($_GET){
        $doctor_id = $_GET['doctor_id'];
        $data = query1("SELECT * FROM doctor WHERE doctor_id = '$doctor_id'");
    }
    if($_POST){
        $doctor_id = $_POST['doctor_id'];
        $doctor_name = $_POST['doctor_name'];
        $doctor_phone = $_POST['doctor_phone'];
        $doctor_add = $_POST['doctor_add'];

        $sql = "UPDATE doctor 
        SET 
        doctor_name = '$doctor_name',
        doctor_phone = '$doctor_phone',
        doctor_add = '$doctor_add'
        WHERE doctor_id = '$doctor_id'";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="doctor_list.php";
        </script>
<?php
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <?include('_navtop.php');?>
        <div class="row" style="padding: 50px;">
            <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-success">
                    <div class="panel-heading">แก้ไขข้อมูล</div>
                    <div class="panel-body">
                        <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                            <div class="form-group">
                                <label>รหัส</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-braille"></i></span>
                                    <input type="text" name="doctor_id" class="form-control" id="doctor_id" value="<?php echo $data->doctor_id ;?>" placeholder="ระบุรหัส" maxlength="200" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ชื่อ-นามสกุล</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="doctor_name" class="form-control" id="doctor_name" value="<?php echo $data->doctor_name ;?>" placeholder="ระบุชื่อ-นามสกุล" maxlength="200" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>โทรศัพท์</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                                    <input type="text" name="doctor_phone" class="form-control" id="doctor_phone" value="<?php echo $data->doctor_phone ;?>" placeholder="ระบุโทรศัพท์" maxlength="10" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ที่อยู่</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                    <textarea class="form-control" name="doctor_add" placeholder="ระบุที่อยู่" maxlength="500"><?php echo $data->doctor_add ;?></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">ตกลง</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>