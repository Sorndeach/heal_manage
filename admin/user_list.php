<?php
    session_start();
    include('../dbconn.inc.php');
    if(isset($_GET['user_id']) && !empty($_GET['user_id'])){
        $user_id = $_GET['user_id'];
        $sql = "DELETE FROM user WHERE user_id = '$user_id'";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('ลบข้อมูลเรียบร้อย');
            window.location.href="user_list.php";
        </script>
<?php
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <?include('_navtop.php');?>
        <div class="row" style="padding: 50px 0px 50px 0px;">
            <div class="col-md-8 col-md-offset-2" style="margin-bottom: 20px;text-align: center;">
                <h2 style="color:#eea236;">จัดการผู้ดูแลระบบ</h2>
            </div>
            <div class="col-md-6 col-md-offset-3" style="margin-bottom: 20px;">
                <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>"  method="post">
                    <div class="input-group">
                        <input type="text" class="form-control" name="filter_name" placeholder="กรอกข้อมูลเพื่อค้นหา...">
                        <span class="input-group-btn">
                            <button class="btn btn-warning" type="submit">ค้นหา</button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="col-md-8 col-md-offset-2" style="margin-bottom: 10px;text-align: right;">
                <a href="user_add.php" class="btn btn-primary"><i class="fa fa-plus"></i> เพิ่ม</a>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <table width="100%" class="table table-striped table-bordered table-hover" id="user-table">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>รหัส</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>เบอร์โทร</th>
                            <th>username</th>
                            <th>แก้ไข/ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $item_per_page = 20;
                        $page_url = "user_list.php";
                        if(isset($_GET["page"])){ 
                            $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
                            if(!is_numeric($page_number)){die('Invalid page number!');} 
                        }else{
                            $page_number = 1; 
                        }

                        $results = $mysqli->query("SELECT COUNT(*) FROM user");
                        $get_total_rows = $results->fetch_row();

                        $total_pages = ceil($get_total_rows[0]/$item_per_page);
                        $page_position = (($page_number-1) * $item_per_page); 

                        // ค้นหาข้อมูล
                        if( isset($_POST['filter_name']) && !empty($_POST['filter_name']) ){
                            $filter_name = $_POST['filter_name'];
                            $sql = "SELECT * FROM user 
                            WHERE user_name LIKE '%$filter_name%'
                            OR user_id LIKE '%$filter_name%'
                            OR user_phone LIKE '%$filter_name%'
                            OR user_user LIKE '%$filter_name%'
                            ORDER BY user_id ASC LIMIT $page_position, $item_per_page";
                        }else{
                            $sql = "SELECT * FROM user ORDER BY user_id ASC LIMIT $page_position, $item_per_page";
                        }
                        $query = $mysqli->query($sql);
                        $i=1;
                        while($data = $query->fetch_object()) :
                    ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $i ;?></td>
                            <td style="text-align: center;"><?php echo $data->user_id?></td>
                            <td><?php echo $data->user_name?></td>
                            <td><?php echo $data->user_phone?></td>
                            <td><?php echo $data->user_user?></td>
                            <td style="text-align: center;">
                                <div class="btn-group btn-group-sm" role="group">
                                    <a class="btn btn-success" href="edit_user.php?user_id=<?php echo $data->user_id?>">แก้ไข</a>
                                    <a class="btn btn-danger" href="user_list.php?user_id=<?php echo $data->user_id?>" onclick="return confirm('คุณต้องการลบข้อมูลของ <?php echo $data->user_name?>')">ลบ</a>
                                </div>
                            </td>
                        </tr>
                    <?php
                        $i++;
                        endwhile;
                    ?>
                        <tr>
                            <td colspan="6" style="text-align: center;font-weight: bold;">รวม <?php echo $i-1 ;?> รายการ</td>
                        </tr>
                        <tr>
                            <td colspan="6"><?php echo paginate($item_per_page, $page_number, $get_total_rows[0], $total_pages, $page_url); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>