<?php
    session_start();
    include('dbconn.inc.php');
    if(isset($_GET['heal_id']) && !empty($_GET['heal_id'])){
        $heal_id = $_GET['heal_id'];
        $sql = "DELETE FROM heal WHERE heal_id = '$heal_id'";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('ลบข้อมูลเรียบร้อย');
            window.location.href="user_list.php";
        </script>
<?php
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <?include('_navtop.php');?>
        <div class="row" style="padding: 50px 0px 50px 0px;">
            <div class="col-md-8 col-md-offset-2" style="margin-bottom: 20px;text-align: center;">
                <h2 style="color:#eea236;">รายการโรค</h2>
            </div>
            <div class="col-md-6 col-md-offset-3" style="margin-bottom: 20px;">
                <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>"  method="post">
                    <div class="input-group">
                        <input type="text" class="form-control" name="filter_name" placeholder="กรอกข้อมูลเพื่อค้นหา...">
                        <span class="input-group-btn">
                            <button class="btn btn-warning" type="submit">ค้นหา</button>
                        </span>
                    </div>
                </form>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <table width="100%" class="table table-striped table-bordered table-hover" id="user-table">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>ชื่อโรค</th>
                            <th>ชื่อแพทย์</th>
                            <th>ชื่อประเภท</th>
                            <th>รายละเอียด</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $item_per_page = 20;
                        $page_url = "index.php";
                        if(isset($_GET["page"])){ 
                            $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
                            if(!is_numeric($page_number)){die('Invalid page number!');} 
                        }else{
                            $page_number = 1; 
                        }

                        $results = $mysqli->query("SELECT COUNT(*) FROM heal");
                        $get_total_rows = $results->fetch_row();

                        $total_pages = ceil($get_total_rows[0]/$item_per_page);
                        $page_position = (($page_number-1) * $item_per_page); 

                        // ค้นหาข้อมูล
                        if( isset($_POST['filter_name']) && !empty( $_POST['filter_name'] ) ){
                            $filter_name = $_POST['filter_name'];
                            $sql = "SELECT heal.*,doctor.doctor_name,medtype.mtype_name 
                            FROM heal
                            LEFT JOIN doctor ON doctor.doctor_id = heal.doctor_id
                            LEFT JOIN medtype ON medtype.mtype_id = heal.mtype_id 
                            WHERE heal.heal_name LIKE '%$filter_name%'
                            OR doctor.doctor_name LIKE '%$filter_name%'
                            OR medtype.mtype_name LIKE '%$filter_name%'
                            OR heal.heal_detail LIKE '%$filter_name%'
                            ORDER BY heal_id ASC LIMIT $page_position, $item_per_page";
                        }else{
                            $sql = "SELECT heal.*,doctor.doctor_name,medtype.mtype_name
                            FROM heal
                            LEFT JOIN doctor ON doctor.doctor_id = heal.doctor_id
                            LEFT JOIN medtype ON medtype.mtype_id = heal.mtype_id
                            ORDER BY doctor_id ASC LIMIT $page_position, $item_per_page";
                        }
                        $query = $mysqli->query($sql);
                        $i=1;
                        $num = $page_position+1;
                        while($data = $query->fetch_object()) : 
                    ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $num; ?></td>
                            <td><?php echo $data->heal_name?></td>
                            <td><?php echo $data->doctor_name?></td>
                            <td><?php echo $data->mtype_name?></td>
                            <td><?php echo $data->heal_detail?></td>
                        </tr>
                    <?php
                        $i++; $num++;
                        endwhile;
                    ?>
                        <tr>
                            <td colspan="5" style="text-align: center;font-weight: bold;">รวม <?php echo $i-1 ;?> รายการ</td>
                        </tr>
                        <tr>
                            <td colspan="5"><?php echo paginate($item_per_page, $page_number, $get_total_rows[0], $total_pages, $page_url); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>