
<?php
    if( !isset($_SESSION['user_id']) ){
        echo "<script>alert('กรุณาล็อคอินด้วยครับ');window.location.href='login.php';</script>";
    }
?>
<div class="row">
    <div class="col-md-12" style="padding:0">
        <img src="../img/head.png" style="width: 100%;height: 120px;">
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
    <nav class="navbar navbar-default" style="">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <img src="#" height="55px;">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="index.php">หน้าแรก</a></li>
                <li><a href="user_list.php">จัดการผู้ดูแลระบบ</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">จัดการข้อมูลรวม <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="doctor_list.php">รายการแพทย์แผนโบราณ</a></li>
                        <li><a href="patient_list.php">รายการคนไข้</a></li>
                        <li><a href="medtype_list.php">รายการประเภทการรักษา</a></li>
                        <li><a href="heal_list.php">รายการข้อมูลโรค</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">รายงาน <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="report_user.php">รายงานผู้ดูแลระบบ</a></li>
                        <li><a href="report_doctor.php">รายงานแพทย์แผนโบราณ</a></li>
                        <li><a href="report_mtype.php">รายงานประเภทการรักษา</a></li>
                        <li><a href="report_heal.php">รายงานข้อมูลโรค</a></li>
                        <li><a href="report_patient.php">รายงานคนไข้   </a></li>
                    </ul>
                </li>
                <?
                    if( !isset($_SESSION['user_id']) ){
                ?>
                        <li><a href="../login.php">เข้าสู่ระบบ</a></li>
                <?
                    }else{
                ?>
                        <li><a href="logout.php">ออกจากระบบ</a></li>
                <?
                    }
                ?>
                
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
    </div>
</div>