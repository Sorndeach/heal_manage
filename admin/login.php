<?php
    session_start();
    include('../dbconn.inc.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;margin-top: 50px;">
        
            <div class="row">
                <div class="col-md-4 col-md-offset-4" style="margin-top: 50px;">
                   <div class="panel panel-warning">
                        <div class="panel-heading">ล็อคอิน</div>
                        <div class="panel-body">
                            <form action="check_login.php" method="post">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label>Username</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                                <input type="text" class="form-control" id="username" name="username" placeholder="ระบุชื่อผู้ใช้เพื่อเข้าระบบ" maxlength="60" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label>Password</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-user-secret"></i></div>
                                                <input type="password" class="form-control" id="password" name="password" placeholder="ระบุรหัสผ่านเพื่อเข้าระบบ" maxlength="60" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-default">ตกลง</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                                    </div>
                                    <div style="clear: both;"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </body>
</html>