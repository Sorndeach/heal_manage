<?php
    session_start();
    include('../dbconn.inc.php');

    if( $_POST ){
        $mtype_id = $_POST['mtype_id'];
        $mtype_name = $_POST['mtype_name'];
        $mtype_detail = $_POST['mtype_detail'];

        $sql = "SELECT * FROM medtype WHERE mtype_id = '$mtype_id'";
        $query = $mysqli->query($sql);
        if($query->num_rows){
            exit("<script>alert('ID ของท่านซ้ำครับ');history.back();</script>");
        }

        $sql = "INSERT INTO medtype VALUES('$mtype_id','$mtype_name','$mtype_detail')";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="medtype_manage.php";
        </script>

<?php
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <?include('_navtop.php');?>
        <div class="row" style="padding: 50px;">
            <div class="col-md-6 col-md-offset-3">
                <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                    <div class="col-md-12">
                        <table style="width: 100%;" class="custom-ta">
                            <tr>
                                <th colspan="2"><span>เพิ่มประเภทการรักษา</span></th>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><span>รหัส :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-braille"></i></span>
                                        <input type="text" name="mtype_id"  class="form-control" id="mtype_id" placeholder="ระบุรหัส"  maxlength="200" required>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><span>ชื่อประเภท :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" name="mtype_name" value="<?php echo isset($data->mtype_name)? $data->mtype_name : ''; ?>" class="form-control" id="mtype_name" placeholder="ระบุชื่อประเภท"  maxlength="200" required>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><span>รายละเอียด :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                        <textarea name="mtype_detail" class="form-control" id="mtype_detail" placeholder="ระบุรายละเอียด" maxlength="500" required></textarea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <button type="submit" class="btn btn-primary">ตกลง</button>
                                    <a href="medtype_manage.php" class="btn btn-warning">ยกเลิก</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
            
        </div>
    </body>
</html>