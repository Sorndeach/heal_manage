<?php
    session_start();
    include('../dbconn.inc.php');

    if($_GET){
        $user_id = $_GET['user_id'];
        $data = query1("SELECT * FROM user WHERE user_id = '$user_id'");
    }
    if($_POST){
        $user_id = $_POST['user_id'];
        $user_name = $_POST['user_name'];
        $user_pass = $_POST['user_pass'];
        $user_phone = $_POST['user_phone'];
        $user_add = $_POST['user_add'];

        $sql = "UPDATE user 
        SET user_name = '$user_name',
        user_pass = '$user_pass',
        user_phone = '$user_phone',
        user_add = '$user_add'
        WHERE user_id = '$user_id'";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="user_list.php";
        </script>
<?php
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <?include('_navtop.php');?>
        <div class="row" style="padding: 50px;">
            <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-success">
                    <div class="panel-heading">แก้ไขผู้ดูแลระบบ</div>
                    <div class="panel-body">
                        <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                            <div class="form-group">
                                <label>รหัส</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-braille"></i></span>
                                    <input type="text" name="user_id" class="form-control" id="user_id" value="<?php echo $data->user_id ;?>" placeholder="ระบุรหัส" maxlength="200" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ชื่อ-นามสกุล</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" name="user_name" class="form-control" id="user_name" value="<?php echo $data->user_name ;?>" placeholder="ระบุชื่อ-นามสกุล" maxlength="200" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ชื่อผู้ใช้</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                    <input type="text" name="user_user" class="form-control" id="user_user" value="<?php echo $data->user_user ;?>" readonly placeholder="รุบุชื่อผู้ใช้" maxlength="30" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>รหัสผ่าน</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <input type="text" name="user_pass" class="form-control" id="user_pass" value="<?php echo $data->user_pass ;?>" placeholder="ระบุรหัสผ่าน" maxlength="30" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>โทรศัพท์</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                                    <input type="text" name="user_phone" class="form-control" id="user_phone" value="<?php echo $data->user_phone ;?>" placeholder="ระบุโทรศัพท์" maxlength="10" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ที่อยู่</label>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                    <textarea class="form-control" name="user_add" placeholder="ระบุที่อยู่" maxlength="500"><?php echo $data->user_add ;?></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">ตกลง</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>