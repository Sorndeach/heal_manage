<?php
    session_start();
    include('../dbconn.inc.php');

    if($_GET){
        $heal_id = $_GET['heal_id'];
        $data = query1("SELECT * FROM heal WHERE heal_id = '$heal_id'");
    }
    if($_POST){
        $heal_id = $_POST['heal_id'];
        $user_id = $_POST['user_id'];
        $doctor_id = $_POST['doctor_id'];
        $mtype_id = $_POST['mtype_id'];
        $heal_date = $_POST['heal_date'];
        $heal_detail = $_POST['heal_detail'];

        $sql = "UPDATE heal 
        SET user_id = '$user_id',
        doctor_id = '$doctor_id',
        mtype_id = '$mtype_id',
        heal_date = '$heal_date',
        heal_detail = '$heal_detail'
        WHERE heal_id = '$heal_id'";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="heal_list.php";
        </script>
<?php
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <div class="container" style="background:rgba(255,255,255,0.9);box-shadow: 0px 0px 15px #828282;">
            <?include('_navtop.php');?>
            <div class="row" style="padding: 50px;">
                <div class="col-md-8 col-md-offset-2">
                   <div class="panel panel-success">
                        <div class="panel-heading">เพิ่มการรักษา</div>
                        <div class="panel-body">
                            <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                                <div class="form-group">
                                    <label>คนไข้</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                        <select class="form-control" name="user_id">
                                            <option>--- เลือกคนไข้---</option>
                                            <?php
                                                $query = query2("SELECT * FROM user WHERE user_status = 'user'");
                                                while($data_select = $query->fetch_object()){
                                            ?>
                                                <option value="<?=$data_select->user_id?>" <?php echo isset($data->user_id) && $data_select->user_id == $data->user_id ? 'selected' : ''; ?> ><?=$data_select->user_name?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>คนไข้</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-user-md"></i></span>
                                        <select class="form-control" name="doctor_id">
                                            <option>--- เลือกแพทย์แผนโบราณ ---</option>
                                            <?php
                                                $query = query2("SELECT * FROM doctor");
                                                while($data_select = $query->fetch_object()){
                                            ?>
                                                <option value="<?=$data_select->doctor_id?>" <?php echo isset($data->doctor_id) && $data_select->doctor_id == $data->doctor_id ? 'selected' : ''; ?> ><?=$data_select->doctor_name?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>คนไข้</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-medkit"></i></span>
                                        <select class="form-control" name="mtype_id">
                                            <option>--- เลือกประเภทการักษา ---</option>
                                            <?php
                                                $query = query2("SELECT * FROM medtype");
                                                while($data_select = $query->fetch_object()){
                                            ?>
                                                <option value="<?=$data_select->mtype_id?>" <?php echo isset($data->mtype_id) && $data_select->mtype_id == $data->mtype_id ? 'selected' : ''; ?> ><?=$data_select->mtype_name?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>วันที่</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="date" name="heal_date" class="form-control" value="<?php echo isset($data->heal_date)? $data->heal_date : ''; ?>" id="heal_date" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>รายละเอียด</label>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                        <textarea class="form-control" name="heal_detail" placeholder="ระบุรายละเอียด" maxlength="500"><?php echo isset($data->heal_detail)? $data->heal_detail : ''; ?></textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="heal_id" value="<?php echo isset( $data->heal_id )? $data->heal_id : ''; ?>">
                                <button type="submit" class="btn btn-primary">ตกลง</button>
                                <button type="reset" class="btn btn-warning">ยกเลิก</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </body>
</html>