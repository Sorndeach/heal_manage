<?php
    session_start();
    include('../dbconn.inc.php');

    if( $_POST ){
        $heal_id = $_POST['heal_id'];
        $heal_name = $_POST['heal_name'];
        $doctor_id = $_POST['doctor_id'];
        $mtype_id = $_POST['mtype_id'];
        $heal_date = $_POST['heal_date'];
        $heal_detail = $_POST['heal_detail'];

        $sql = "SELECT * FROM heal WHERE heal_id = '$heal_id'";
        $query = $mysqli->query($sql);
        if($query->num_rows){
            exit("<script>alert('ID ของท่านซ้ำครับ');history.back();</script>");
        }

        $sql = "INSERT INTO heal VALUES('$heal_id','$heal_name','$doctor_id','$mtype_id','$heal_date','$heal_detail')";
        @$mysqli->query($sql) or die($mysqli->error);
?>
        <script type="text/javascript">
            alert('บันทึกข้อมูลเรียบร้อย');
            window.location.href="index.php";
        </script>

<?php
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <? include('_head.php');?>
    </head>
    <body style="background-color:#FFF;">
        <?include('_navtop.php');?>
        <div class="row" style="padding: 50px;">
            <div class="col-md-6 col-md-offset-3">
                <form id="frm1"  action="<? $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">
                    <div class="col-md-12">
                        <table style="width: 100%;" class="custom-ta">
                            <tr>
                                <th colspan="2"><span>เพิ่มข้อมูลโรค</span></th>
                            </tr>
                            <tr>
                                <td style="width: 50px;"><span>รหัส :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-braille"></i></span>
                                        <input type="text" name="heal_id"  class="form-control" id="heal_id" placeholder="ระบุรหัส"  maxlength="200" required>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><span>ชื่อโรค :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-area-chart"></i></span>
                                        <input type="text" name="heal_name" class="form-control" id="heal_name" placeholder="ระบุชื่อโรค"  maxlength="200" required>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><span>แพทย์แผนโบราณ :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <select class="form-control" name="doctor_id">
                                            <option value="">:::เลือกแพทย์แผนโบราณ:::</option>
                                            <?php
                                                $query = query2("SELECT * FROM doctor");
                                                while($data = $query->fetch_object()) {
                                            ?>
                                                 <option value="<?php echo $data->doctor_id; ?>"><?php echo $data->doctor_name; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><span>ประเภทการรักษา :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-sticky-note-o"></i></span>
                                        <select class="form-control" name="mtype_id">
                                            <option value="">:::เลือกประเภทการรักษา:::</option>
                                            <?php
                                                $query = query2("SELECT * FROM medtype");
                                                while($data = $query->fetch_object()) {
                                            ?>
                                                 <option value="<?php echo $data->mtype_id; ?>"><?php echo $data->mtype_name; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;"><span>วันที่ :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="date" name="heal_date" class="form-control" id="heal_date" maxlength="200" required>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><span>รายละเอียด :</span></td>
                                <td>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                        <textarea name="heal_detail" class="form-control" id="heal_detail" placeholder="ระบุรายละเอียด" maxlength="500" required></textarea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <button type="submit" class="btn btn-primary">ตกลง</button>
                                    <a href="medtype_manage.php" class="btn btn-warning">ยกเลิก</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
            
        </div>
    </body>
</html>